CREATE DATABASE node36_mysql_appfood;

USE node36_mysql_appfood;

--bảng users
CREATE TABLE users(
	user_id INT PRIMARY KEY AUTO_INCREMENT,
	full_name VARCHAR(50),
	email VARCHAR(50),
	password varchar(255)
);

INSERT INTO users (full_name, email, password) VALUES
('John Doe', 'johndoe@example.com', 'password1'),
('Alice Smith', 'alicesmith@example.com', 'password2'),
('Bob Johnson', 'bobjohnson@example.com', 'password3'),
('Eve Wilson', 'evewilson@example.com', 'password4'),
('Charlie Brown', 'charliebrown@example.com', 'password5'),
('Grace Davis', 'gracedavis@example.com', 'password6'),
('Oliver Lee', 'oliverlee@example.com', 'password7'),
('Sophia Moore', 'sophiamoore@example.com', 'password8'),
('Liam Wilson', 'liamwilson@example.com', 'password9'),
('Emma Turner', 'emmaturner@example.com', 'password10');
('Apolo Aimstrong', 'apolo@example.com', 'password11');

--bảng restaurant
CREATE table restaurant(
	res_id INT PRIMARY KEY AUTO_INCREMENT,
	res_name VARCHAR(50),
	image VARCHAR(255),
	descs varchar(255)
);

INSERT INTO restaurant (res_name, image, descs) VALUES
('Restaurant A', 'image1.jpg', 'A cozy place with great food.'),
('Restaurant B', 'image2.jpg', 'Delicious dishes in a beautiful setting.'),
('Restaurant C', 'image3.jpg', 'Perfect spot for a romantic dinner.'),
('Restaurant D', 'image4.jpg', 'Authentic cuisine from around the world.'),
('Restaurant E', 'image5.jpg', 'Family-friendly atmosphere and menu.'),
('Restaurant F', 'image6.jpg', 'A trendy place for foodies.'),
('Restaurant G', 'image7.jpg', 'Elegant dining with a view.'),
('Restaurant H', 'image8.jpg', 'Gourmet experience for food enthusiasts.'),
('Restaurant I', 'image9.jpg', 'Casual dining with a twist.'),
('Restaurant J', 'image10.jpg', 'Local favorites and international delights.');

--bảng like_res
create table like_res(
	like_id INT PRIMARY KEY AUTO_INCREMENT,
	user_id INT,
	FOREIGN KEY(user_id) REFERENCES users(user_id),
	
	res_id INT,
	FOREIGN KEY(res_id) REFERENCES restaurant(res_id),
	
	date_like DATE
);

INSERT INTO like_res (user_id, res_id, date_like) VALUES
(1, 1, '2023-10-01'),
(2, 1, '2023-10-02'),
(3, 2, '2023-10-03'),
(4, 3, '2023-10-04'),
(5, 4, '2023-10-05'),
(6, 5, '2023-10-06'),
(7, 6, '2023-10-07'),
(8, 7, '2023-10-08'),
(9, 8, '2023-10-09'),
(10, 9, '2023-10-10');

--bảng rate_res
CREATE TABLE rate_res(
	rate_id INT PRIMARY KEY AUTO_INCREMENT,
	user_id INT,
	FOREIGN KEY(user_id) REFERENCES users(user_id),
	
	res_id INT,
	FOREIGN KEY(res_id) REFERENCES restaurant(res_id),
	
	amount INT,
	date_rate DATE
);

INSERT INTO rate_res (user_id, res_id, amount, date_rate) VALUES
(1, 1, 5, '2023-10-01'),
(2, 1, 4, '2023-10-02'),
(3, 2, 5, '2023-10-03'),
(4, 3, 4, '2023-10-04'),
(5, 4, 5, '2023-10-05'),
(6, 5, 4, '2023-10-06'),
(7, 6, 5, '2023-10-07'),
(8, 7, 4, '2023-10-08'),
(9, 8, 5, '2023-10-09'),
(10, 9, 4, '2023-10-10');

--bảng food_type
CREATE TABLE food_type(
	type_id INT PRIMARY KEY AUTO_INCREMENT,
	type_name VARCHAR(50)
);

INSERT INTO food_type (type_name) VALUES
('Appetizers'),
('Main Dishes'),
('Desserts'),
('Beverages'),
('Vegetarian'),
('Seafood'),
('Italian'),
('Chinese'),
('Mexican'),
('Japanese');

--bảng food
CREATE table food(
	food_id int PRIMARY key AUTO_INCREMENT,
	food_name varchar(50),
	image VARCHAR(255),
	price FLOAT,
	descs VARCHAR(255),
	type_id INT,
	FOREIGN KEY(type_id) REFERENCES food_type(type_id)
);

INSERT INTO food (food_name, image, price, descs, type_id) VALUES
('Caesar Salad', 'image1.jpg', 8.99, 'Fresh and crisp Caesar salad', 1),
('Steak', 'image2.jpg', 19.99, 'Juicy and tender steak', 2),
('Cheesecake', 'image3.jpg', 6.99, 'Creamy and rich cheesecake', 3),
('Soda', 'image4.jpg', 2.99, 'Variety of soft drinks', 4),
('Vegetable Stir-Fry', 'image5.jpg', 12.99, 'Healthy and delicious', 5),
('Grilled Salmon', 'image6.jpg', 17.99, 'Perfectly grilled salmon', 6),
('Spaghetti Carbonara', 'image7.jpg', 14.99, 'Classic Italian pasta', 7),
('Kung Pao Chicken', 'image8.jpg', 13.99, 'Spicy Chinese dish', 8),
('Tacos', 'image9.jpg', 10.99, 'Authentic Mexican street food', 9),
('Sushi Rolls', 'image10.jpg', 16.99, 'Fresh and flavorful sushi', 10);

--bảng sub_food
create table sub_food(
	sub_id INT PRIMARY KEY AUTO_INCREMENT,
	sub_name VARCHAR(50),
	sub_price FLOAT,
	food_id INT,
	FOREIGN key (food_id) REFERENCES food(food_id)
);

INSERT INTO sub_food (sub_name, sub_price, food_id) VALUES
('Garlic Bread', 3.99, 1),
('Mashed Potatoes', 2.99, 2),
('Chocolate Mousse', 4.99, 3),
('Lemonade', 1.99, 4),
('Grilled Tofu', 5.99, 5),
('Shrimp Scampi', 7.99, 6),
('Tiramisu', 5.99, 7),
('Egg Drop Soup', 2.99, 8),
('Guacamole', 1.99, 9),
('Sashimi Platter', 8.99, 10);

--bảng orders
create table orders(
	order_id INT PRIMARY KEY AUTO_INCREMENT,
	user_id INT,
	FOREIGN KEY(user_id) REFERENCES users(user_id),
	
	food_id INT,
	FOREIGN KEY(food_id) REFERENCES food(food_id),
	
	amount INT,
	code VARCHAR(255),
	
	arr_sub_id VARCHAR(255)
);

INSERT INTO orders (user_id, food_id, amount, code, arr_sub_id) VALUES
(1, 1, 2, 'ORDER123', '1,2'),
(2, 2, 1, 'ORDER124', '3'),
(3, 3, 3, 'ORDER125', '4,5,6'),
(4, 4, 1, 'ORDER126', '7'),
(5, 5, 2, 'ORDER127', '8,9'),
(6, 6, 2, 'ORDER128', '10,1'),
(7, 7, 1, 'ORDER129', '2'),
(8, 8, 4, 'ORDER130', '3,4,5,6'),
(9, 9, 1, 'ORDER131', '7'),
(10, 10, 2, 'ORDER132', '8,9');

--phần thực hành bài tập
--câu 1: Tìm 5 người đã like nhà hàng nhiều nhất

SELECT u.user_id, u.full_name, COUNT(u.user_id) FROM users as u
JOIN like_res as lr ON u.user_id = lr.user_id
GROUP BY u.user_id, u.full_name
ORDER BY COUNT(u.user_id) DESC
LIMIT 5;

--câu 2: Tìm 2 nhà hàng có lượt like nhiều nhất

SELECT r.res_id, r.res_name, COUNT(r.res_id) FROM restaurant as r
JOIN like_res as lr ON r.res_id = lr.res_id
GROUP BY r.res_id, r.res_name
ORDER BY COUNT(r.res_id) DESC
LIMIT 2;

--câu 3: Tìm người đã đặt hàng nhiều nhất

SELECT u.user_id, u.full_name, COUNT(u.user_id) FROM users as u
JOIN orders as o ON u.user_id = o.user_id
GROUP BY u.user_id, u.full_name
ORDER BY COUNT(u.user_id) DESC
LIMIT 1;

--câu 4: Tìm người dùng không hoạt động trong hệ thống (không đặt hàng, không like, không đánh giá nhà hàng)

SELECT u.user_id, u.full_name FROM users AS u
LEFT JOIN orders AS o ON u.user_id = o.user_id
LEFT JOIN like_res AS lr ON u.user_id = lr.user_id
LEFT JOIN rate_res AS rr ON u.user_id = rr.user_id
WHERE o.user_id IS NULL AND lr.user_id IS NULL AND rr.user_id IS NULL;